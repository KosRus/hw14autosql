﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Data.SqlClient;
using MySql.Data.MySqlClient;
using NUnit.Framework;


namespace SQLCSharpPractic
{
    [TestFixture]
    public class Tests
    {    /// <summary>
         /// Проверка таблицы Persons
         /// </summary>
        [OneTimeSetUp]
        public void StartTestCheckDataFromPersonsInDataBase()
        {
            SqlConnector.ConnectToCatalog("HomeWorkSQLRuskykhKonstantin");
        }

        [TestCase("Dnipro", 0, 4)]
        [TestCase("David", 1, 1)]
        [TestCase("Telles", 8, 2)]
        [TestCase("43", 18, 3)]
        [TestCase("Glasgow", 19, 4)]

        public void CheckDataFromPersonsInDataBase(string expResult,
            int rowsIndex, int columnIndex)
        {
            SqlConnector sqlConnector = new SqlConnector("sa", "965729");
            DataTable result = sqlConnector.Execute
                ("SELECT * FROM Persons;");
            Assert.AreEqual(expResult, result.Rows[rowsIndex].
                ItemArray[columnIndex].
                ToString());
        }
        [OneTimeTearDown]
        public void CloseTestCheckDataFromPersonsInDataBase()
        {
            SqlConnector sqlConnector = new SqlConnector("sa", "965729");
            sqlConnector.Close();
        }
        /// <summary>
        /// Проверка таблицы Orders
        /// </summary>
        [OneTimeSetUp]
        public void StartTestCheckDataFromOrdersInDataBase()
        {
            SqlConnector.ConnectToCatalog("HomeWorkSQLRuskykhKonstantin");
        }
        [TestCase("23", 0, 2)]
        [TestCase("18", 43, 0)]

        public void CheckDataFromOrdersInDataBase(string expResult,
            int rowsIndex, int columnIndex)
        {

            SqlConnector sqlConnector = new SqlConnector("sa", "965729");
            DataTable result2 = sqlConnector.Execute("SELECT * FROM Orders");

            Assert.AreEqual(expResult, result2.Rows[rowsIndex].
                    ItemArray[columnIndex].
                    ToString());

        }
        [OneTimeTearDown]
        public void CloseTestStartTestCheckDataFromOrdersInDataBase()
        {
            SqlConnector sqlConnector = new SqlConnector("sa", "965729");
            sqlConnector.Close();
        }
        /// <summary>
        /// Проверка запроса всех клиентов из Манчестера
        /// </summary>
        [OneTimeSetUp]
        public void StartCheckAllPersonsFromManchesterInPersonsTable()
        {
            SqlConnector.ConnectToCatalog("HomeWorkSQLRuskykhKonstantin");
        }

        [TestCase("Henderson", 0, 0)]
        [TestCase("Rashford", 1, 0)]
        [TestCase("Greenwood", 2, 0)]


        public void CheckAllPersonsFromManchesterInPersonsTable(string expResult,
            int rowsIndex, int columnIndex)
        {
            SqlConnector sqlConnector = new SqlConnector("sa", "965729");
            DataTable result = sqlConnector.Execute
                ("SELECT LastName FROM Persons WHERE City like '%Manchester%';");
            Assert.AreEqual(expResult, result.Rows[rowsIndex].
                ItemArray[columnIndex].
                ToString());
        }
        [OneTimeTearDown]

        public void CloseCheckAllPersonsFromManchesterInPersonsTable()
        {
            SqlConnector sqlConnector = new SqlConnector("sa", "965729");
            sqlConnector.Close();
        }
        /// <summary>
        /// Проверка запроса какой клиент сделал самый большой заказ
        /// </summary>
        [OneTimeSetUp]
        public void CheckPersonWhoDidMaxValueOrder()
        {
            SqlConnector.ConnectToCatalog("HomeWorkSQLRuskykhKonstantin");
        }

        [TestCase("Cavani", 0, 2)]

        public void CheckPersonWhoDidMaxValueOrder(string expResult,
            int rowsIndex, int columnIndex)
        {
            SqlConnector sqlConnector = new SqlConnector("sa", "965729");
            DataTable result = sqlConnector.Execute
                ("SELECT * FROM Persons WHERE UserId = " +
                "(SELECT Id FROM Orders WHERE Sum = (SELECT MAX(Sum) FROM Orders));");
            Assert.AreEqual(expResult, result.Rows[rowsIndex].
                ItemArray[columnIndex].
                ToString());
        }
        [OneTimeTearDown]

        public void CloseCheckPersonWhoDidMaxValueOrder()
        {
            SqlConnector sqlConnector = new SqlConnector("sa", "965729");
            sqlConnector.Close();
        }
        /// <summary>
        /// Проверка запроса кто из клиентов сделал заказов на сумму больше чем 100
        /// </summary>
        [OneTimeSetUp]
        public void StartCheckPersonWhoHaveSumOfOrdersMoreThan100()
        {
            SqlConnector.ConnectToCatalog("HomeWorkSQLRuskykhKonstantin");
        }

        [TestCase("Cavani", 0, 0)]
        [TestCase("1472,06", 0, 1)]
        [TestCase("DeGea", 12, 0)]
        [TestCase("101", 12, 1)]

        public void CheckPersonWhoHaveSumOfOrdersMoreThan100(string expResult,
            int rowsIndex, int columnIndex)
        {
            SqlConnector sqlConnector = new SqlConnector("sa", "965729");
            DataTable result = sqlConnector.Execute
                ("SELECT Persons.LastName, SUM(Orders.Sum) AS TotalSalesOrderAmount " +
                 "FROM Persons FULL OUTER JOIN Orders " +
                 "ON Persons.UserId = Orders.Id " +
                 "GROUP BY Persons.LastName " +
                 "HAVING SUM(Orders.Sum) > 100 " +
                 "ORDER BY TotalSalesOrderAmount DESC; ");
            Assert.AreEqual(expResult, result.Rows[rowsIndex].
                ItemArray[columnIndex].
                ToString());
        }
        [OneTimeTearDown]

        public void CloseCheckPersonWhoHaveSumOfOrdersMoreThan100()
        {
            SqlConnector sqlConnector = new SqlConnector("sa", "965729");
            sqlConnector.Close();
        }
        /// <summary>
        /// Проверка запроса кто из клиентов делал заказы
        /// </summary>
        [OneTimeSetUp]
        public void CheckCheckAllClientWhoHadOrders()
        {
            SqlConnector.ConnectToCatalog("HomeWorkSQLRuskykhKonstantin");
        }

        [TestCase("1", 0, 0)]
        [TestCase("19", 18, 0)]
        [TestCase("10", 9, 0)]


        public void CheckAllClientWhoHadOrders(string expResult,
            int rowsIndex, int columnIndex)
        {
            SqlConnector sqlConnector = new SqlConnector("sa", "965729");
            DataTable result = sqlConnector.Execute
                ("SELECT DISTINCT Id From Orders;");
            Assert.AreEqual(expResult, result.Rows[rowsIndex].
                ItemArray[columnIndex].
                ToString());
        }
        [OneTimeTearDown]

        public void CloseCheckAllClientWhoHadOrders()
        {
            SqlConnector sqlConnector = new SqlConnector("sa", "965729");
            sqlConnector.Close();
        }
        /// <summary>
        /// Проверка запроса на какие суммы и кто делал заказы
        /// </summary>
        [OneTimeSetUp]
        public void StartCheckAllOrdersSumAndWhoMadeThem()
        {
            SqlConnector.ConnectToCatalog("HomeWorkSQLRuskykhKonstantin");
        }

        [TestCase("Bailly", 0, 1)]
        [TestCase("Bailly", 2, 1)]
        [TestCase("67,8", 0, 2)]
        [TestCase("56", 2, 2)]
        [TestCase("Wan-bissaka", 43, 1)]
        [TestCase("45", 43, 2)]


        public void CheckAllOrdersSumAndWhoMadeThem(string expResult,
            int rowsIndex, int columnIndex)
        {
            SqlConnector sqlConnector = new SqlConnector("sa", "965729");
            DataTable result = sqlConnector.Execute
                ("SELECT Persons.FirstName, Persons.LastName, Orders.Sum " +
                 "FROM Persons RIGHT JOIN Orders " +
                 "ON Persons.UserId = Orders.Id " +
                 "ORDER BY Persons.LastName; ");
            Assert.AreEqual(expResult, result.Rows[rowsIndex].
                ItemArray[columnIndex].
                ToString());
        }
        [OneTimeTearDown]

        public void CloseCheckAllOrdersSumAndWhoMadeThem()
        {
            SqlConnector sqlConnector = new SqlConnector("sa", "965729");
            sqlConnector.Close();
        }
    }
}

