﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace SQLCSharpPractic
{
    public class SqlConnector
    {
        private readonly SqlCredential _credential;
        static private string _connectionString;

        public SqlConnector(string login, string password)
        {

            //convert password
            var credential = new SecureString();
            for (var i = 0; i < password.Length; i++)
                credential.InsertAt(i, password[i]);
            credential.MakeReadOnly();
            //save your Credential for requests
            _credential = new SqlCredential(login, credential);

        }
        public static void ConnectToCatalog(string catalogName)
        {
            _connectionString = "Data Source=DESKTOP-3UKE6PQ; " +
                $"Initial Catalog= {catalogName}; ";
        }
        public DataTable Execute(string sqlRequest)
        {
            var dataSet = new DataSet();
            using (var sqlConnection = new SqlConnection(_connectionString, _credential))
            {
                sqlConnection.Open();
                using (var sqlCommand = new SqlCommand(sqlRequest, sqlConnection))
                {
                    sqlCommand.CommandText = sqlRequest;

                    var adapter = new SqlDataAdapter(sqlCommand);

                    adapter.Fill(dataSet);
                }
            }

            if (sqlRequest.StartsWith("SELECT") && dataSet.Tables[0].Rows[0] != null)
            {
                return dataSet.Tables[0];
            }
            else
            {
                throw new Exception("Your are not write");
            }
        }

        internal void Close()
        {
            throw new NotImplementedException();
        }
        
    }
}
